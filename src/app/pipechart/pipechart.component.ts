import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ApexChart, ApexNonAxisChartSeries, ApexResponsive, ChartComponent } from 'ng-apexcharts';
import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { StatisticalDTO } from '../model/statistical';
import { StatisticalService } from '../statistical.service';

export type ChartOptions = {
  series: ApexNonAxisChartSeries;
  chart: ApexChart;
  responsive: ApexResponsive[];
  labels: any;

};
@Component({
  selector: 'app-pipechart',
  templateUrl: './pipechart.component.html',
  styleUrls: ['./pipechart.component.css']
})
export class PipechartComponent implements OnInit {
  statisticalDTO: StatisticalDTO[];
  @ViewChild("chart") chart: ChartComponent;
  public chartOptions: Partial<ChartOptions>;
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public statisticalService: StatisticalService,
    ) {

    this.chartOptions = {
      series: [44, 55, 13],
      chart: {
        width: 380,
        type: "pie"
      },
      labels: ["Nhà trọ", "Chung cư", "Nhà nguyên căn"],
      responsive: [
        {
          breakpoint: 480,
          options: {
            chart: {
              width: 200
            },
            legend: {
              position: "bottom"
            }
          }
        }
      ]
    };

  }

  ngOnInit() {
    // console.log("ccc", this.data.month);
    
   //  this.getStatistical1(this.data.year,(parseInt(this.data.month) +1));
  }

  getStatistical1( year: any, month: any){
// this...getStatistical1()
    this.statisticalService.getStatistical1(this.data.year, this.data.month).subscribe(
      (data) => {
        console.log("cccccccccc",data);
      }, err => {
        console.log("Loi", err);
      }
    )
  }

}
