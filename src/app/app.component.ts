

import { Chart } from 'chart.js';

import { Component, ViewChild } from '@angular/core';
import { MomentDateAdapter, MAT_MOMENT_DATE_ADAPTER_OPTIONS } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatDatepicker } from '@angular/material/datepicker';
import * as _moment from 'moment';
import { Moment } from 'moment';
import { FormControl } from '@angular/forms';
import * as _rollupMoment from 'moment';
import { MatDialog } from '@angular/material/dialog';
import { StatisticalService } from './statistical.service';
import { PipechartComponent } from './pipechart/pipechart.component';
import { StatisticalDTO } from './model/statistical';
const moment = _rollupMoment || _moment;
export const MY_FORMATS = {
  parse: {
    dateInput: 'YYYY',
  },
  display: {
    dateInput: 'YYYY',
    monthYearLabel: 'YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'YYYY',
  },
};
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
    },

    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
  ]
})
export class AppComponent {
  title = 'testthongke1';
  date = new FormControl(moment());
  year: number = 2021;
  motel_type:any = "";
  statisticalDTO: StatisticalDTO[];
  total: any[] = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
  month: any[] = [];

  canvas: any;
  ctx: any;
  @ViewChild("mychart") mychart;
  constructor(
    public statisticalService: StatisticalService,
    public dialog: MatDialog,

  ) {this.getStatistical(this.year);}

  ngAfterViewInit(  statisticalService: StatisticalService) {
    
    this.canvas = this.mychart.nativeElement;
    this.ctx = this.canvas.getContext("2d");
    let subPerf = new Chart(this.ctx, {
      type: "line",
      data: {
        labels: ["Tháng 1", "Tháng 2", "Tháng 3", "Tháng 4", "Tháng 5", "Tháng 6", "Tháng 7","Tháng 8", "Tháng 9", "Tháng 10", "Tháng 11", "Tháng 12"],
        datasets: [
          {
            label: "Tổng doanh thu",
            backgroundColor: "rgba(0, 255, 255, 0.5)",
            borderColor: "rgb(0,0,255)",
            fill: true,
            data: this.total
          }
        ]
      },
      options: {
        responsive: true,
        onClick: event => {
          let point = Chart.helpers.getRelativePosition(event, subPerf.chart);
          let xIndex = subPerf.scales["x-axis-0"].getValueForPixel(point.x);
          let label = subPerf.data.labels[xIndex];
          console.log(label + " at index " + xIndex);
          this.test(xIndex);
        },
        scales: {
          yAxes: [
            {
              ticks: {
                beginAtZero: true
              }
            }
          ]
        }
      }
    });
  }

  generateDataChart(){
    this.canvas = this.mychart.nativeElement;
    this.ctx = this.canvas.getContext("2d");
    let subPerf = new Chart(this.ctx, {
      type: "line",
      data: {
        labels: ["Tháng 1", "Tháng 2", "Tháng 3", "Tháng 4", "Tháng 5", "Tháng 6", "Tháng 7","Tháng 8", "Tháng 9", "Tháng 10", "Tháng 11", "Tháng 12"],
        datasets: [
          {
            label: "Tổng doanh thu",
            backgroundColor: "rgba(0, 255, 255, 0.5)",
            borderColor: "rgb(0,0,255)",
            fill: true,
            data: this.total
          }
        ]
      },
      options: {
        responsive: true,
        onClick: event => {
          let point = Chart.helpers.getRelativePosition(event, subPerf.chart);
          let xIndex = subPerf.scales["x-axis-0"].getValueForPixel(point.x);
          let label = subPerf.data.labels[xIndex];
          console.log(label + " at index " + xIndex);
          this.test(xIndex);
        },
        scales: {
          yAxes: [
            {
              ticks: {
                beginAtZero: true
              }
            }
          ]
        }
      }
    });
  }
  test(index) {
    console.log("xxxxxxxx", index);
  }

  public getStatistical(year: any) {
    this.statisticalService.getStatistical(year).subscribe(
      data => {
        this.statisticalDTO = data
        this.total = [];
        this.month = [];
        data.forEach(element => {
          this.total.push(element.total);
          this.month.push("Tháng " + element.month)
        });
        console.log(this.total);
  
        this.generateDataChart()
      }
  
  
    )
  }

//  choose year start
chosenYearHandler(normalizedYear: Moment, datepicker: MatDatepicker<Moment>) {
  const ctrlValue = this.date.value;
  ctrlValue.year(normalizedYear.year());
  this.date.setValue(ctrlValue);

  this.year = this.date.value.year();
  console.log(this.date.value.year());

  this.getStatistical(this.date.value.year())
  datepicker.close();
}
// choose yaer end

openDialog(evnt) {

  console.log(evnt);

  const dialogRef = this.dialog.open(PipechartComponent, {
    width: '400px',
    data:{
      motel_type: this.motel_type,
      month: this.month,
      yaer: this.year
    }
    });

  dialogRef.afterClosed().subscribe(result => {
    console.log('The dialog was closed');
    // this.animal = result;
  });
}
}
