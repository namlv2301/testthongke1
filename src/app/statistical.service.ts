import { Observable } from 'rxjs';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
const cabecera = {headers: new HttpHeaders({'Content-Type': 'application/json'})};  

@Injectable({
  providedIn: 'root'
})
export class StatisticalService {

  api = 'http://localhost:8080/api/admin/';
  constructor(private http: HttpClient) { }
  public chart(data: any): Observable<any> {
    return this.http.post<any>(this.api + 'statistical', data);
  }
  public getStatistical(year: any): Observable<any> {
    return this.http.get<any>(this.api + `statistical/${year}`,cabecera);
  }
  public getStatistical1(year: any ,month: any): Observable<any> {
    return this.http.get<any>(this.api + `statistical/${year}/${month}`,cabecera);
  }

}

